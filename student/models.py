from lec.models import User, Unit, Project
from django.db import models


# Create your models here
# eg BBIT
class Program(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    # modules = models.ManyToManyField(Module, related_name="program_modules", blank=True)

    def __str__(self):
        return self.name
        
# BBIT Yr 1
class Module(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    code = models.CharField(max_length=30)
    name = models.CharField(max_length=50)
    program = models.ForeignKey(Program, on_delete=models.CASCADE, null=True)
    # common units
    units = models.ManyToManyField(
        Unit, related_name='common_units', blank=True)

    def __str__(self):
        return self.name


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, limit_choices_to={'is_student': True})
    reg_num = models.CharField(max_length=15)
    program = models.ForeignKey(Program, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.reg_num

class Submission(models.Model):
    student_user = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='student')
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name='project')
    title = models.CharField(max_length=140)
    link = models.CharField(max_length=140, blank=True)
    upload_file = models.FileField()
    additional_notes = models.TextField(max_length=500)
    is_submit = models.BooleanField(default=False)

    def __str__(self):
        return self.title
