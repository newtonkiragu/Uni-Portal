from django.contrib import admin
from .models import Student, Program, Module

class ProgramAdmin(admin.ModelAdmin):
    exclude = ('user',)
    list_display = ('code', 'name', 'user')

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

class StudentAdmin(admin.ModelAdmin):
    list_display = ('reg_num', 'user', 'get_program')

    list_select_related = ('user',)

    def get_program(self, instance):
        return instance.program
    get_program.short_description = 'program'
    
class ModuleAdmin(admin.ModelAdmin):
    exclude = ('user',)
    list_display = ('code', 'name', 'user')

    filter_horizontal = ('units',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()  

# Register your models here.
admin.site.register(Student, StudentAdmin)
admin.site.register(Program,ProgramAdmin)
admin.site.register(Module, ModuleAdmin)
