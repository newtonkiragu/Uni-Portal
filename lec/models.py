from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime as dt

# Create your models here.
class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_lecturer = models.BooleanField(default=False)
        
    def __str__(self):
        return self.username

class Department(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=50)

    class Meta:
        managed=False

    def __str__(self):
        return self.name

class Unit(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    code = models.CharField(max_length=30)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Lecturer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, limit_choices_to={'is_lecturer': True})
    staff_id = models.CharField(max_length=15)
    department = models.ManyToManyField(Department, related_name="lec_department")
    units = models.ManyToManyField(Unit, related_name="lec_units", blank=True)

    class Meta:
        managed=False


    def __str__(self):
        return self.staff_id

class Assessment(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE)
    category = models.CharField(max_length=50)

    class Meta:
        managed=False


    def __str__(self):
        return self.category
        
# reps assignment
class Project(models.Model):
    lec = models.ForeignKey(
        Lecturer, on_delete=models.CASCADE, related_name='lec_project')
    title = models.CharField(max_length=140)
    link = models.CharField(max_length=140, blank=True)
    upload_file = models.FileField(blank=True)
    additional_notes = models.TextField(max_length=500, blank=True)
    assessment_type = models.ForeignKey(
        Assessment, related_name="assessment", on_delete="models.CASCADE", null=True)
    unit = models.ForeignKey(
        Unit, related_name="unit", on_delete="models.CASCADE", null=True)
    due_date = models.DateField(auto_now=False, blank=True)
    due_time = models.TimeField(auto_now=False, blank=True)
    is_submitProject = models.BooleanField(default=False)

    class Meta:
        managed=False


    def __str__(self):
        return self.title     



