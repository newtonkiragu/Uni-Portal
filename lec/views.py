from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from .models import User, Lecturer, Department, Assessment, Unit, Project



# Create your views here.
def is_lecturer(user):
    return user.is_lecturer

@login_required(login_url='/accounts/login/')
@user_passes_test(is_lecturer)
def lecture(request):
    user = request.user
    user_id = user.id
    lec = User.objects.get(id=user_id)
    print(lec.lecturer.staff_id)
    units = lec.lecturer.units.all()
    print(units)
    print(user)
    return render(request, 'lec.html', {'units':units})
    # projects = lec.lecturer.projects.all()
    # print(projects)
    # return render(request, 'lec.html', {'projects':projects})

@user_passes_test(is_lecturer)
def units(request, unit_id):
    user = request.user
    user_id = user.id
    lec = User.objects.get(id=user_id)
    units = lec.lecturer.units.all()
    unit = Unit.objects.get(id=unit_id)
    return render(request, 'units.html', {'unit':unit})

# def projects(request, project_id):
#     project = Project.objects.get(id=project_id)
#     return render(request, 'projects.html', {'project', project})
    