from django.contrib import admin
from .models import User, Lecturer, Department, Assessment, Unit
from django.contrib.auth.admin import UserAdmin,UserCreationForm, UserChangeForm

class CustomUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal Info', {'fields': ('first_name', 'last_name', 'email')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_lecturer', 'is_student', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'is_lecturer', 'is_student', 'password1', 'password2')}
        ),
    )

    list_display = ('id', 'username', 'first_name', 'last_name', 'is_lecturer', 'is_student', 'is_staff', 'is_active')
    ordering = ['id']

class LecturerAdmin(admin.ModelAdmin):
    list_display = ('staff_id', 'user', 'get_departments')

    list_select_related = ('user',)

    filter_horizontal = ('department', 'units')

    def get_departments(self, instance):
        return ",".join([str(dept.name) for dept in instance.department.all()])
    get_departments.short_description = 'department' 

class DepartmentAdmin(admin.ModelAdmin):
    exclude = ('user',)
    list_display = ('code', 'name', 'user')

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

class AssessmentAdmin(admin.ModelAdmin):
    exclude = ('user',)
    list_display = ('category',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()   

class UnitAdmin(admin.ModelAdmin):
    exclude = ('user',)
    list_display = ('code','name', 'user')

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()               

# Register your models here.

admin.site.register(User, CustomUserAdmin)
admin.site.register(Lecturer, LecturerAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Assessment, AssessmentAdmin)
admin.site.register(Unit, UnitAdmin)