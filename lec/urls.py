from django.conf.urls import url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # lec dashboards
    url(r'^$', views.lecture, name='lecturer'),
    url(r'^units/(\d+)$', views.units, name='units'),


]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)